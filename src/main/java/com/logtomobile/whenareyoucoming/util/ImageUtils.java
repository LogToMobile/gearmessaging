package com.logtomobile.whenareyoucoming.util;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.Log;
import android.util.LruCache;

import static com.google.common.base.Preconditions.checkArgument;
import static com.google.common.base.Preconditions.checkNotNull;

/**
 * @author Marcin Przepiórkowski
 */
public final class ImageUtils {
    protected static LruCache<String, Bitmap> sImageCache;

    static {
        final int maxMemory = (int) (Runtime.getRuntime().maxMemory());
        final int cacheSize = maxMemory / 4;
        Log.d("image_cache", "max memory: " + maxMemory);
        Log.d("image_cache", "cache size: " + cacheSize);

        sImageCache = new LruCache<String, Bitmap>(cacheSize) {
            @Override
            protected int sizeOf(String key, Bitmap value) {
                return value.getRowBytes() * value.getHeight();
            }
        };
    }

    private ImageUtils() {
    }

    /**
     * Retrieves specified bitmap from the cache and returns it.
     *
     * @param key   bitmap key, cannot be null or empty
     * @return      Bitmap object if there's a bitmap with specified key in the cache or null otherwise
     */
    public static Bitmap getFromCache(String key) {
        checkNotNull(key, "key cannot be null");
        checkArgument(!key.isEmpty(), "key cannot be empty");

        return sImageCache.get(key);
    }

    /**
     * Adds a bitmap to the cache.
     *
     * @param key   bitmap key, cannot be null or empty
     * @param bmp   bitmap object, cannot be null
     */
    public static void addToCache(String key, Bitmap bmp) {
        checkNotNull(key, "key cannot be null");
        checkNotNull(bmp, "bmp cannot be null");

        if (getFromCache(key) == null) {
            sImageCache.put(key, bmp);
        }
    }

    /**
     * Combines two given bitmaps using specified Porter-Duff Xfermode.
     *
     * @param src   source image (main image)
     * @param dst   destination image (puzzle image)
     * @param mode  Xfermode
     * @return      combined bitmap
     */
    public static Bitmap combineImages(Bitmap src, Bitmap dst, PorterDuff.Mode mode) {
        Bitmap output = Bitmap.createBitmap(dst.getWidth(), dst.getHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(output);

        Paint paint = new Paint();
        canvas.drawBitmap(src, 0, 0, paint);
        paint.setXfermode(new PorterDuffXfermode(mode));
        canvas.drawBitmap(dst, 0, 0, paint);

        return output;
    }

    public static Bitmap transformToCircle( Bitmap bmp) {
        checkNotNull(bmp, "bmp cannot be null");

        Paint p = new Paint();
        p.setColor(0xff000000);
        p.setStyle(Paint.Style.FILL_AND_STROKE);
        p.setAntiAlias(true);

        int w = bmp.getWidth();
        int h = bmp.getHeight();
        int radius, bmpX, bmpY, bmpWidth, bmpHeight;
        if (w > h) {
            radius = h / 2;
            bmpX = (w - h) / 2;
            bmpY = 0;
            bmpHeight = h;
            bmpWidth = h;
        } else {
            radius = w / 2;
            bmpX = 0;
            bmpWidth = w;
            bmpY = (h - w) / 2;
            bmpHeight = w;
        }

        Bitmap circleImage = Bitmap.createBitmap(radius * 2, radius * 2, Bitmap.Config.ARGB_8888);

        Canvas c = new Canvas(circleImage);
        c.drawCircle(bmpWidth / 2, bmpHeight / 2, radius, p);
        p.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SRC_IN));

        c.drawBitmap(Bitmap.createBitmap(bmp, bmpX, bmpY, bmpWidth, bmpHeight), 0.0f, 0.0f, p);

        return circleImage;
    }
}