package com.logtomobile.whenareyoucoming;

import android.app.Application;
import android.content.Context;

import com.google.common.eventbus.EventBus;
import com.logtomobile.whenareyoucoming.service.WhenAreYouComingProviderService;

import java.util.HashMap;

/**
 *@author Tomasz Trybała
 */
public class WhenAreYouComingApplication extends Application {
    private static Context sContext;
    private static EventBus sBus;
    private static HashMap<Integer, WhenAreYouComingProviderService.HelloAccessoryProviderConnection> mApplicationConnectionMap;
    private static int mConnectionId;

    @Override
    public void onCreate() {
        super.onCreate();

        sContext = this;
        sBus = new EventBus("Event Bus");
    }

    /**
     * Returns application context;
     *
     * @return context
     */
    public static Context getAppContext() {
        return sContext;
    }

    /**
     * Returns application event bus.
     *
     * @return application event bus
     */
    public static EventBus getApplicationEventBus() {
        return sBus;
    }

    public static HashMap<Integer, WhenAreYouComingProviderService.HelloAccessoryProviderConnection> getMap(){
        return mApplicationConnectionMap;
    }

    public static void setMap(HashMap<Integer, WhenAreYouComingProviderService.HelloAccessoryProviderConnection> map){
        mApplicationConnectionMap = map;
    }


    public static int getId(){
        return mConnectionId;
    }

    public static void setId(int id){
        mConnectionId = id;
    }

}
