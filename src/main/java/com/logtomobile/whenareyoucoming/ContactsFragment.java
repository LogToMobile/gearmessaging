package com.logtomobile.whenareyoucoming;

import android.app.Fragment;
import android.content.ComponentName;
import android.content.ContentUris;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.AssetFileDescriptor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.util.Base64;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.eventbus.Subscribe;
import com.logtomobile.whenareyoucoming.db.ContactsDataSource;
import com.logtomobile.whenareyoucoming.db.enitity.Person;
import com.logtomobile.whenareyoucoming.event.EditPersonEvent;
import com.logtomobile.whenareyoucoming.event.ListEvent;
import com.logtomobile.whenareyoucoming.service.WhenAreYouComingProviderService;
import com.logtomobile.whenareyoucoming.util.ImageUtils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;

/**
 * @author Tomasz Trybała
 */
public class ContactsFragment extends Fragment {
    private class PersonListAdapter extends ArrayAdapter<Person> {
        private final Context mContext;
        private final ArrayList<Person> mData;
        private final int mLayout;

        private class ViewHolder {
            public TextView mTxtvName;
            public ImageView mImgvPhoto;
            public ImageView mImgvCircle;
            public ImageView mImgvOverlay;
        }

        public PersonListAdapter(Context context, ArrayList<Person> data, int layout) {
            super(context, layout, data);
            mContext = context;
            mData = data;
            mLayout = layout;
        }

        private void loadBitmap(final String contactId, final ImageView imageView) {
            if (ImageUtils.getFromCache(contactId) != null) {
                imageView.setImageBitmap(ImageUtils.getFromCache(contactId));
            } else {
                AsyncTask<Void, Void, Void> imageLoader = new AsyncTask<Void, Void, Void>() {
                    private Bitmap mBitmap = null;

                    @Override
                    protected Void doInBackground(Void... voids) {
                        try {
//                            InputStream inputStream = ContactsContract.Contacts.openContactPhotoInputStream(
//                                    getActivity().getContentResolver(),
//                                    ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI,
//                                            Long.valueOf(contactId)));

                            InputStream inputStream = openDisplayPhoto(Long.valueOf(contactId));

                            if (inputStream != null) {
                                mBitmap = BitmapFactory.decodeStream(inputStream);
                                inputStream.close();
                            } else {
                                mBitmap = BitmapFactory.decodeResource(getResources(), R.drawable.default_photo);
                            }
                            mBitmap = ImageUtils.transformToCircle(mBitmap);
                        } catch (IOException e) {
                            //file not found
                            //contact photo will be default
                        }
                        return null;
                    }

                    @Override
                    protected void onPostExecute(Void aVoid) {
                        if (mBitmap != null) {
                            imageView.setImageBitmap(mBitmap);
                        }
                    }
                };

                imageLoader.execute();
            }
        }


        @SuppressWarnings("ConstantConditions")
        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                convertView = inflater.inflate(mLayout, viewGroup, false);
                viewHolder = new ViewHolder();

                viewHolder.mTxtvName = (TextView) convertView.findViewById(R.id.txtvName);
                viewHolder.mImgvPhoto = (ImageView) convertView.findViewById(R.id.imgvPersonPhoto);
                viewHolder.mImgvCircle = (ImageView) convertView.findViewById(R.id.imgvCircleContact);
                viewHolder.mImgvOverlay = (ImageView) convertView.findViewById(R.id.imgvOverlay);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            Person person = mData.get(position);
            viewHolder.mImgvCircle.setSelected(person.isSelected());
            viewHolder.mImgvCircle.setVisibility(mIsActionMode ?
                    View.VISIBLE :
                    View.GONE);
            viewHolder.mTxtvName.setText(person.getName());
            viewHolder.mImgvPhoto.setTag(person.getContactId());
            viewHolder.mImgvOverlay.setVisibility(View.INVISIBLE);
            loadBitmap(person.getContactId(), viewHolder.mImgvPhoto);
            return convertView;
        }
    }

    //region views
    private GridView mGridView;
    private TextView mEmptyList;
    private ProgressBar mProgressBar;
    //endregion

    //region constants
    private static final String CONTACTS_START = "contacts_start";
    private static final String CONTACTS_RECORD = "contacts_name_%s_number_%s_image_%s";
    private static final String NUMBER = "number";
    private static final String NAME = "name";
    private static final String POSITION = "position";
    //endregion

    //region variables
    private ArrayList<Person> mData = new ArrayList<Person>();
    private PersonListAdapter mListAdapter;
    protected Object mActionMode;
    private boolean mIsActionMode;
    private WhenAreYouComingProviderService mService;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mService = ((WhenAreYouComingProviderService.LocalBinder) iBinder).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }
    };

    private ActionMode.Callback mActionModeCallback = new ActionMode.Callback() {

        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            MenuInflater inflater = mode.getMenuInflater();
            inflater.inflate(R.menu.delete_contact, menu);
            return true;
        }

        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.menuDeleteContact:
                    deleteSelected();
                    mode.finish();
                    return true;
                default:
                    return false;
            }
        }

        public void onDestroyActionMode(ActionMode mode) {
            mActionMode = null;
            mIsActionMode = false;
            mListAdapter.notifyDataSetChanged();
            setView();
        }
    };
    //endregion

    private void findViews(View root) {
        mGridView = (GridView) root.findViewById(R.id.gvContacts);
        mEmptyList = (TextView) root.findViewById(R.id.txtvEmptyList);
        mProgressBar = (ProgressBar) root.findViewById(R.id.progressBar);
    }

    private void setAdapter() {
        mListAdapter = new PersonListAdapter(getActivity(), mData, R.layout.contact_row);
        mGridView.setAdapter(mListAdapter);
    }

    private void deleteSelected() {
        ArrayList<Person> newData = new ArrayList<Person>();
        for (Person person : mData) {
            if (!person.isSelected()) {
                newData.add(person);
            }
        }

        mData.clear();
        for (Person person : newData) {
            mData.add(person);
        }

        mListAdapter.notifyDataSetChanged();
        setView();
        sendToGear();
    }

    private InputStream openDisplayPhoto(long contactId) {
        Uri contactUri = ContentUris.withAppendedId(ContactsContract.Contacts.CONTENT_URI, contactId);
        Uri displayPhotoUri = Uri.withAppendedPath(contactUri, ContactsContract.Contacts.Photo.DISPLAY_PHOTO);
        try {
            AssetFileDescriptor fd =
                    getActivity().getContentResolver().openAssetFileDescriptor(displayPhotoUri, "r");
            return fd.createInputStream();
        } catch (IOException e) {
            return null;
        }
    }

    private void setListeners() {
        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {

                if (mActionMode != null) {
                    return false;
                }

                mIsActionMode = true;

                mActionMode = getActivity()
                        .startActionMode(mActionModeCallback);
                mData.get(position).setSelected(true);
                mListAdapter.notifyDataSetChanged();
                return true;
            }
        });

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (mIsActionMode) {
                    mData.get(i).setSelected(!mData.get(i).isSelected());
                    mListAdapter.notifyDataSetChanged();
                } else {
                    Person person = mData.get(i);

                    Bundle arguments = new Bundle();
                    arguments.putString(NUMBER, person.getNumber());
                    arguments.putString(NAME, person.getName());
                    arguments.putInt(POSITION, i);

                    EditContactDialog dialog = new EditContactDialog();
                    dialog.setArguments(arguments);
                    dialog.show(getFragmentManager(), "");
                }
            }
        });
    }

    private void setView() {
        if (mData.isEmpty()) {
            mGridView.setVisibility(View.GONE);
            mEmptyList.setVisibility(View.VISIBLE);
        } else {
            mGridView.setVisibility(View.VISIBLE);
            mEmptyList.setVisibility(View.GONE);
        }
    }

    private boolean isUniqueNumber(String number) {
        for (Person person : mData) {
            if (person.getNumber().equals(number) || number.equals(person.getNumber())) {
                return false;
            }
        }
        return true;
    }

    private void loadData() {
        AsyncTask<Void, Void, Void> loaderTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                ContactsDataSource eds = ContactsDataSource.getInstance();

                mData.clear();
                mData.addAll(eds.getAll());

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mListAdapter.notifyDataSetChanged();
                mProgressBar.setVisibility(View.GONE);
                setView();
            }
        };
        loaderTask.execute();
    }

    private void saveData() {
        AsyncTask<Void, Void, Void> personDataExecutor = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {

                ContactsDataSource ds = ContactsDataSource.getInstance();
                ds.deleteAll();
                for (Person person : mData) {
                    ds.insert(person);
                }

                return null;
            }
        };
        personDataExecutor.execute();
    }

    private void sendToGear() {
        AsyncTask<Void, Void, Void> sender = new AsyncTask<Void, Void, Void>() {
            private Bitmap mPhoto = null;

            @Override
            protected void onPreExecute() {
                if (mService != null) {
                    mService.sendMessage(CONTACTS_START);
                }
            }

            @Override
            protected Void doInBackground(Void... voids) {
                for (Person person : mData) {
                    String image = "0";
                    try {
                        InputStream inputStream = openDisplayPhoto(
                                Long.valueOf(person.getContactId()));

                        if (inputStream != null) {
                            mPhoto = BitmapFactory.decodeStream(inputStream);
                            inputStream.close();

                            mPhoto = Bitmap.createScaledBitmap(mPhoto, 200, 200, true);
                            mPhoto = ImageUtils.transformToCircle(mPhoto);
                            ByteArrayOutputStream stream = new ByteArrayOutputStream();
                            mPhoto.compress(Bitmap.CompressFormat.PNG, 100, stream);

                            byte[] byteArray = stream.toByteArray();
                            image = Base64.encodeToString(byteArray, Base64.DEFAULT);
                        }
                    } catch (IOException e) {
                        //file not found
                        //contact photo will be default
                    }

                    if (mService != null) {
                        mService.sendMessage(String.format(CONTACTS_RECORD,
                                person.getName(),
                                person.getNumber(),
                                image));
                    }
                }
                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                if (mPhoto != null) {
                    mPhoto.recycle();
                }
            }
        };

        sender.execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_contacts, container, false);
        findViews(root);
        setAdapter();
        setListeners();

        return root;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();

        if (getActivity() != null && getActivity().getActionBar() != null) {
            getActivity().getActionBar().setTitle(getResources().getString(R.string.contacts_title_activity));
        }

        sendToGear();
    }

    @Override
    public void onStart() {
        super.onStart();
        WhenAreYouComingApplication.getApplicationEventBus().register(this);
        WhenAreYouComingApplication.getAppContext().bindService(
                new Intent(WhenAreYouComingApplication.getAppContext(), WhenAreYouComingProviderService.class),
                serviceConnection,
                Context.BIND_AUTO_CREATE);
    }

    @Override
    public void onStop() {
        super.onStop();
        WhenAreYouComingApplication.getApplicationEventBus().unregister(this);
        WhenAreYouComingApplication.getAppContext().unbindService(serviceConnection);
        saveData();
    }

    public void addContact(Person person) {
        if (person.getNumber() != null && !person.getNumber().equals("")) {
            if (isUniqueNumber(person.getNumber())) {
                mData.add(person);
                mListAdapter.notifyDataSetChanged();
                setView();
                sendToGear();
            } else {
                Toast.makeText(WhenAreYouComingApplication.getAppContext(),
                        "Person with the same number already exists!",
                        Toast.LENGTH_SHORT).show();
            }
        } else {
            Toast.makeText(WhenAreYouComingApplication.getAppContext(),
                    "No phone number in this contact!",
                    Toast.LENGTH_SHORT).show();
        }
    }

    @Subscribe
    public void refreshList(ListEvent event) {
        mListAdapter.notifyDataSetChanged();
        setView();
        sendToGear();
    }

    @Subscribe
    public void editContact(EditPersonEvent event) {
        if (mData.get(event.getPosition()) != null) {
            mData.get(event.getPosition()).setName(event.getName());
        }

        mListAdapter.notifyDataSetChanged();
        setView();
        sendToGear();
    }
}
