package com.logtomobile.whenareyoucoming.db.table;

import android.database.sqlite.SQLiteDatabase;

/**
 *@author Tomasz Trybała
 */
public class Messages {
    private Messages(){}

    public static final String TABLE_NAME = "messages";

    public static final class Column{
        public static final String MESSAGE_ID = "id";
        public static final String TEXT = "text";
    }

    public static final class ColumnID {
        public static final int MESSAGE_ID = 0;
        public static final int TEXT = 1;
    }

    public static final String[] ALL_COLUMNS = {
            Column.MESSAGE_ID,
            Column.TEXT
    };

    public static void createTable(SQLiteDatabase db) {
        String sql = String.format(
                "CREATE TABLE IF NOT EXISTS %s (" +
                        "%s INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +       // message id
                        "%s TEXT, ",                               // text
                TABLE_NAME,
                Column.MESSAGE_ID,
                Column.TEXT
        );

        db.execSQL(sql);
    }
}
