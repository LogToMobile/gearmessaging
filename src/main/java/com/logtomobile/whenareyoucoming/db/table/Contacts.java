package com.logtomobile.whenareyoucoming.db.table;

import android.database.sqlite.SQLiteDatabase;

/**
 *@author Tomasz Trybała
 */
public class Contacts {
    private Contacts(){}

    public static final String TABLE_NAME = "contacts";

    public static final class Column{
        public static final String PERSON_ID = "id";
        public static final String NAME = "name";
        public static final String NUMBER = "number";
        public static final String CONTACT_ID = "contactId";
    }

    public static final class ColumnID {
        public static final int PERSON_ID = 0;
        public static final int NAME = 1;
        public static final int NUMBER = 2;
        public static final int CONTACT_ID = 3;
    }

    public static final String[] ALL_COLUMNS = {
            Column.PERSON_ID,
            Column.NAME,
            Column.NUMBER,
            Column.CONTACT_ID
    };

    public static void createTable(SQLiteDatabase db) {
        String sql = String.format(
                "CREATE TABLE IF NOT EXISTS %s (" +
                        "%s INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, " +       // person id
                        "%s TEXT, " +                               // name
                        "%s TEXT, " +                               // number
                        "%s TEXT, ",                                // contactId
                TABLE_NAME,
                Column.PERSON_ID,
                Column.NAME,
                Column.NUMBER,
                Column.CONTACT_ID
        );

        db.execSQL(sql);
    }
}
