package com.logtomobile.whenareyoucoming.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.logtomobile.whenareyoucoming.db.enitity.Person;
import com.logtomobile.whenareyoucoming.db.table.Contacts;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Tomasz Trybała
 */
public class ContactsDataSource {
    private static ContactsDataSource sDataSource;
    private SQLiteDatabase mDatabase;

    private ContactsDataSource() {
        DbHelper dbHelper = DbHelper.getInstance();
        mDatabase = dbHelper.getDatabase();
    }

    private Person cursorToContacts(Cursor c) {
        int id;
        String name, number, contactId;

        id = c.getInt(Contacts.ColumnID.PERSON_ID);
        name = c.getString(Contacts.ColumnID.NAME);
        number = c.getString(Contacts.ColumnID.NUMBER);
        contactId = c.getString(Contacts.ColumnID.CONTACT_ID);

        return new Person(id, name, number, contactId);
    }

    public static ContactsDataSource getInstance() {
        if (sDataSource == null) {
            sDataSource = new ContactsDataSource();
        }

        return sDataSource;
    }

    public Person get(int personId) {
        String where = String.format("%s = %d", Contacts.Column.PERSON_ID, personId);
        Cursor c = mDatabase.query(Contacts.TABLE_NAME, Contacts.ALL_COLUMNS, where, null, null, null, null);
        c.moveToFirst();

        if (c.getCount() > 0) {
            Person person = cursorToContacts(c);
            c.close();

            return person;
        } else {
            c.close();
            return Person.INVALID_PERSON;
        }
    }

    public List<Person> getAll() {
        Cursor c = mDatabase.query(Contacts.TABLE_NAME, Contacts.ALL_COLUMNS, null, null, null, null, null);
        c.moveToFirst();

        List<Person> contacts = new LinkedList<Person>();

        if (c.getCount() > 0) {
            do {
                contacts.add(cursorToContacts(c));
            } while (c.moveToNext());
        } else {
            c.close();
        }

        return contacts;
    }

    public void insert(Person person) {
        ContentValues values = new ContentValues();
        values.put(Contacts.Column.NAME, person.getName());
        values.put(Contacts.Column.NUMBER, person.getNumber());
        values.put(Contacts.Column.CONTACT_ID, person.getContactId());

        mDatabase.insertWithOnConflict(Contacts.TABLE_NAME, null, values,
                SQLiteDatabase.CONFLICT_REPLACE);
    }

    public void deleteAll(){
        mDatabase.delete(Contacts.TABLE_NAME, null, null);
    }
}
