package com.logtomobile.whenareyoucoming.db;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.logtomobile.whenareyoucoming.WhenAreYouComingApplication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * @author Marcin Przepiórkowski
 */
/*package*/ final class DbHelper extends SQLiteOpenHelper {
    private static final String DB_NAME = "db.sqlite";
    private static final int DB_VERSION = 1;

    private static DbHelper sDbHelper;
    private SQLiteDatabase mDatabase;

    private void copyDataBase(String dbname) throws IOException {
        @SuppressLint ("SdCardPath") File dbDir = new File("/data/data/com.logtomobile.whenareyoucoming/databases");

        if (!dbDir.exists()) {
            //noinspection ResultOfMethodCallIgnored
            dbDir.mkdir();
        }

        InputStream myInput = WhenAreYouComingApplication.getAppContext().getAssets().open(dbname);
        @SuppressLint ("SdCardPath") String outFileName = "/data/data/com.logtomobile.whenareyoucoming/databases/" + dbname;

        if (!(new File(outFileName)).exists()) {
            OutputStream myOutput = new FileOutputStream(outFileName);

            byte[] buffer = new byte[1024];
            int length;

            while ((length = myInput.read(buffer)) > 0) {
                myOutput.write(buffer, 0, length);
            }

            myOutput.flush();
            myOutput.close();
        }

        myInput.close();
    }

    private DbHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);

        try {
            copyDataBase(DB_NAME);
            mDatabase = getWritableDatabase();
        } catch (IOException ioe) {
            System.out.println("ioe: " + ioe.getMessage());
            context.openOrCreateDatabase(DB_NAME, Context.MODE_PRIVATE, null);
        }
    }

    public static DbHelper getInstance() {
        if (sDbHelper == null) {
            sDbHelper = new DbHelper(WhenAreYouComingApplication.getAppContext());
        }

        return sDbHelper;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onCreate(db);
    }

    public SQLiteDatabase getDatabase() {
        return mDatabase;
    }
}