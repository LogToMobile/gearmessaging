package com.logtomobile.whenareyoucoming.db.enitity;

/**
 *@author Tomasz Trybała
 */
public class Message {
    private int mId;
    private String mText;

    public static Message INVALID_MESSAGE = new Message(-1, "");

    public Message(int id, String text){
        mId = id;
        mText = text;
    }

    public Message(String text){
        mText = text;
    }

    public int getId(){return mId;}

    public String getText(){return mText;}

    public void setMessage(String message){
        mText = message;
    }
}
