package com.logtomobile.whenareyoucoming.db.enitity;

/**
 *@author Tomasz Trybała
 */
public class Person {
    private int mId;
    private String mName, mNumber, mContactId;
    private boolean mIsSelected;

    public static Person INVALID_PERSON = new Person(-1, "", "", "");

    public Person(int id, String name, String number, String contactId){
        mId = id;
        mName = name;
        mNumber = number;
        mContactId = contactId;
    }

    public Person(String name, String number, String contactId){
        mName = name;
        mNumber = number;
        mContactId = contactId;
    }

    public String getContactId(){
        return mContactId;
    }

    public int getId() {
        return mId;
    }

    public String getName() {
        return mName;
    }

    public String getNumber() {
        return mNumber;
    }

    public void setSelected(boolean selected){
        mIsSelected = selected;
    }

    public void setName(String name){
        mName = name;
    }

    public boolean isSelected(){
        return mIsSelected;
    }
}
