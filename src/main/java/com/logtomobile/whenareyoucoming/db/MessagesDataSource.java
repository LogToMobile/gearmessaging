package com.logtomobile.whenareyoucoming.db;

import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.logtomobile.whenareyoucoming.db.enitity.Message;
import com.logtomobile.whenareyoucoming.db.table.Messages;

import java.util.LinkedList;
import java.util.List;

/**
 *@author Tomasz Trybała
 */
public class MessagesDataSource {
    private static MessagesDataSource sDataSource;
    private SQLiteDatabase mDatabase;

    private MessagesDataSource(){
        DbHelper dbHelper = DbHelper.getInstance();
        mDatabase = dbHelper.getDatabase();
    }

    private Message cursorToMessages(Cursor c) {
        int id;
        String text;

        id = c.getInt(Messages.ColumnID.MESSAGE_ID);
        text = c.getString(Messages.ColumnID.TEXT);

        return new Message(id, text);
    }

    public static MessagesDataSource getInstance() {
        if (sDataSource == null) {
            sDataSource = new MessagesDataSource();
        }

        return sDataSource;
    }

    public Message get(int messageId) {
        String where = String.format("%s = %d", Messages.Column.MESSAGE_ID, messageId);
        Cursor c = mDatabase.query(Messages.TABLE_NAME, Messages.ALL_COLUMNS, where, null, null, null, null);
        c.moveToFirst();

        if (c.getCount() > 0) {
            Message message = cursorToMessages(c);
            c.close();

            return message;
        } else {
            c.close();
            return Message.INVALID_MESSAGE;
        }
    }

    public List<Message> getAll() {
        Cursor c = mDatabase.query(Messages.TABLE_NAME, Messages.ALL_COLUMNS, null, null, null, null, null);
        c.moveToFirst();

        List<Message> messages = new LinkedList<Message>();

        if (c.getCount() > 0) {
            do {
                messages.add(cursorToMessages(c));
            } while (c.moveToNext());
        } else {
            c.close();
        }

        return messages;
    }

    public void insert(Message message) {
        ContentValues values = new ContentValues();
        values.put(Messages.Column.TEXT, message.getText());

        mDatabase.insertWithOnConflict(Messages.TABLE_NAME, null, values,
                SQLiteDatabase.CONFLICT_REPLACE);
    }

    public void deleteAll(){
        mDatabase.delete(Messages.TABLE_NAME, null, null);
    }
}
