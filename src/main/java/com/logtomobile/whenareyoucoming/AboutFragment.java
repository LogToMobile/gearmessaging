package com.logtomobile.whenareyoucoming;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * @author Tomasz Trybała
 */
public class AboutFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        if(getActivity() != null && getActivity().getActionBar() != null){
            getActivity().getActionBar().setTitle(getResources().getString(R.string.about_title_activity));
        }

        return inflater.inflate(R.layout.fragment_about, container, false);
    }
}
