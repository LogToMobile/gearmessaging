package com.logtomobile.whenareyoucoming;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.content.res.Configuration;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.IBinder;
import android.provider.ContactsContract;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.widget.DrawerLayout;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.logtomobile.whenareyoucoming.db.enitity.Person;
import com.logtomobile.whenareyoucoming.service.WhenAreYouComingProviderService;

import java.io.File;

/**
 * @author Tomasz Trybała
 */
public class MainActivity extends Activity implements AddController {
    //region variables
    private static final String DEST_PATH = "/storage/emulated/legacy/test1.amr";
    private Uri uriContact;
    private String contactID;
    private static final int REQUEST_CODE_PICK_CONTACTS = 1;

    private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    public static boolean isUp = false;
    private int mFragmentState = 0; // 0-contacts, 1-templates, 2-settings, 3-about
    private Fragment mFragment;

    public int mTransId;

    private WhenAreYouComingProviderService mFTService;

    private ServiceConnection mFTConnection = new ServiceConnection() {
        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mFTService = null;
        }

        @Override
        public void onServiceConnected(ComponentName arg0, IBinder service) {
            mFTService = ((WhenAreYouComingProviderService.LocalBinder) service).getService();
            mFTService.registerFileAction(getFileAction());
        }
    };
    //endregion

    protected void setDrawer() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);

        ImageView imgvContacts = (ImageView) findViewById(R.id.imgvContacts);
        ImageView imgvTemplates = (ImageView) findViewById(R.id.imgvTemplates);
        ImageView imgvAbout = (ImageView) findViewById(R.id.imgvAbout);

        imgvContacts.setOnClickListener(getListener(0, new ContactsFragment()));

        imgvTemplates.setOnClickListener(getListener(1, new MessagesFragment()));

        imgvAbout.setOnClickListener(getListener(3, new AboutFragment()));

    }

    private View.OnClickListener getListener(final int fragmentState, final Fragment fragment) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mFragmentState != fragmentState) {
                    mFragmentState = fragmentState;
                    FragmentManager fragmentManager = getFragmentManager();
                    FragmentTransaction transaction = fragmentManager.beginTransaction();
                    mFragment = fragment;
                    transaction.replace(R.id.flContainer, mFragment);
                    transaction.commit();
                    invalidateOptionsMenu();
                }
                mDrawerLayout.closeDrawers();
                Log.e("test", "test");
            }
        };
    }

    protected void setActionBar() {
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(
                this,
                mDrawerLayout,
                R.drawable.ic_drawer,
                R.string.drawer_1,
                R.string.drawer_2
        ) {

            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
            }

            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
            }
        };

        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
            getActionBar().setHomeButtonEnabled(true);
        }
    }

    private String retrieveContactNumber() {
        String contactNumber = null;

        Cursor cursorID = getContentResolver().query(uriContact,
                new String[]{ContactsContract.Contacts._ID},
                null, null, null);

        if (cursorID.moveToFirst()) {
            contactID = cursorID.getString(cursorID.getColumnIndex(ContactsContract.Contacts._ID));
        }

        cursorID.close();

        Cursor cursorPhone = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                new String[]{ContactsContract.CommonDataKinds.Phone.NUMBER},

                ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = ? AND " +
                        ContactsContract.CommonDataKinds.Phone.TYPE + " = " +
                        ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE,

                new String[]{contactID},
                null
        );

        if (cursorPhone.moveToFirst()) {
            contactNumber = cursorPhone.getString(cursorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        }

        cursorPhone.close();
        return contactNumber;
    }

    private String retrieveContactName() {
        String contactName = null;

        Cursor cursor = getContentResolver().query(uriContact, null, null, null, null);

        if (cursor.moveToFirst()) {
            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        }

        cursor.close();
        return contactName;
    }

    private void prepareService() {
        if (!Environment.getExternalStorageState().equals(Environment.MEDIA_MOUNTED)) {
            Toast.makeText(getApplicationContext(), " No SDCARD Present", Toast.LENGTH_SHORT).show();
            finish();
        } else {
            String mDirPath = Environment.getExternalStorageDirectory() + File.separator + "FTSampleProvider";
            File file = new File(mDirPath);
            if (file.mkdirs()) {
                Toast.makeText(getApplicationContext(), " Stored in " + mDirPath, Toast.LENGTH_LONG).show();
            }
        }

        getApplicationContext().bindService(new Intent(getApplicationContext(), WhenAreYouComingProviderService.class),
                this.mFTConnection, Context.BIND_AUTO_CREATE);
        Intent i = getIntent();
        if (i != null && i.getAction() != null) {
            if (i.getAction().equalsIgnoreCase("incomingFT")) {
                getFileAction().onTransferRequested(i.getIntExtra("tx", -1), i.getStringExtra("fileName"));
            }
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setDrawer();
        setActionBar();

        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        mFragment = new ContactsFragment();
        transaction.replace(R.id.flContainer, mFragment);
        transaction.commit();

        prepareService();

        isUp = true;
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        isUp = false;
    }

    @Override
    protected void onPause() {
        super.onPause();
        isUp = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        isUp = true;
    }

    @Override
    protected void onStop() {
        super.onStop();
        WhenAreYouComingApplication.getApplicationEventBus().unregister(this);
        isUp = false;
    }

    @Override
    protected void onStart() {
        super.onStart();
        WhenAreYouComingApplication.getApplicationEventBus().register(this);
        isUp = true;
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        if (mFragmentState == 0) {
            getMenuInflater().inflate(R.menu.add_contact, menu);
        } else if (mFragmentState == 1) {
            getMenuInflater().inflate(R.menu.add_message, menu);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (mDrawerToggle.onOptionsItemSelected(item)) {
            return true;
        }

        int id = item.getItemId();
        if (id == R.id.menuAddContact) {
            startActivityForResult(new Intent(Intent.ACTION_PICK,
                    ContactsContract.Contacts.CONTENT_URI), REQUEST_CODE_PICK_CONTACTS);
        }
        if (id == R.id.menuAddMessage) {
            TemplateDialog dialog = new TemplateDialog();
            dialog.show(getFragmentManager(), "");
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        prepareService();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == REQUEST_CODE_PICK_CONTACTS) {
            uriContact = data.getData();
            String contactNumber = retrieveContactNumber();
            String contactName = retrieveContactName();
            Person person = new Person(contactName, contactNumber, contactID);
            addPerson(person);
        }
    }

    private WhenAreYouComingProviderService.FileAction getFileAction() {
        return new WhenAreYouComingProviderService.FileAction() {
            @Override
            public void onError(final String errorMsg, final int errorCode) {
            }

            @Override
            public void onProgress(final long progress) {
            }

            @Override
            public void onTransferComplete(String path) {
            }

            @Override
            public void onTransferRequested(int id, String path) {
                mTransId = id;
                mFTService.receiveFile(mTransId, DEST_PATH, true);
            }
        };
    }

    @Override
    public void addPerson(Person p) {
        if (mFragment instanceof ContactsFragment) {
            ((ContactsFragment) mFragment).addContact(p);
        }
    }
}
