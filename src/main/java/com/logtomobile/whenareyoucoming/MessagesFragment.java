package com.logtomobile.whenareyoucoming;

import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.common.eventbus.Subscribe;
import com.logtomobile.whenareyoucoming.db.MessagesDataSource;
import com.logtomobile.whenareyoucoming.db.enitity.Message;
import com.logtomobile.whenareyoucoming.event.AddMessageEvent;
import com.logtomobile.whenareyoucoming.event.ListEvent;
import com.logtomobile.whenareyoucoming.service.WhenAreYouComingProviderService;

import java.util.ArrayList;

/**
 * @author Tomasz Trybała
 */
public class MessagesFragment extends Fragment {
    private class MessageListAdapter extends ArrayAdapter<Message> {
        private final Context mContext;
        private final ArrayList<Message> mData;
        private final int mLayout;

        private class ViewHolder {
            public ImageView mBtnRemove;
            public TextView mTxtvText;
        }

        public MessageListAdapter(Context context, ArrayList<Message> data, int layout) {
            super(context, layout, data);
            mContext = context;
            mData = data;
            mLayout = layout;
        }

        @SuppressWarnings("ConstantConditions")
        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {
            final ViewHolder viewHolder;
            if (convertView == null) {
                LayoutInflater inflater = LayoutInflater.from(mContext);
                convertView = inflater.inflate(mLayout, viewGroup, false);
                viewHolder = new ViewHolder();

                viewHolder.mTxtvText = (TextView) convertView.findViewById(R.id.txtvMessageText);
                viewHolder.mBtnRemove = (ImageView) convertView.findViewById(R.id.btnRemove);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            Message message = mData.get(position);
            viewHolder.mTxtvText.setText(message.getText());
            viewHolder.mBtnRemove.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    mData.remove(position);
                    WhenAreYouComingApplication.getApplicationEventBus().post(new ListEvent());
                }
            });
            return convertView;
        }
    }

    private ListView mListView;
    private TextView mEmptyList;
    private ProgressBar mProgressBar;
    private ArrayList<Message> mData = new ArrayList<Message>();
    private MessageListAdapter mListAdapter;
    private static final String TEMPLATE_START = "template_start";
    private static final String TEMPLATE_RECORD = "template_%s";
    private static final String MESSAGE = "message";
    private static final String POSITION = "position";
    private WhenAreYouComingProviderService mService;
    private ServiceConnection serviceConnection = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            mService = ((WhenAreYouComingProviderService.LocalBinder) iBinder).getService();
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            mService = null;
        }
    };

    private void findViews(View root) {
        mListView = (ListView) root.findViewById(R.id.lvMessages);
        mEmptyList = (TextView) root.findViewById(R.id.txtvEmptyList);
        mProgressBar = (ProgressBar) root.findViewById(R.id.progressBar);
    }

    private void setAdapter() {
        mListAdapter = new MessageListAdapter(getActivity(), mData, R.layout.message_row);
        mListView.setAdapter(mListAdapter);
    }

    private void setView() {
        if (mData.isEmpty()) {
            mListView.setVisibility(View.GONE);
            mEmptyList.setVisibility(View.VISIBLE);
        } else {
            mListView.setVisibility(View.VISIBLE);
            mEmptyList.setVisibility(View.GONE);
        }
    }

    private void setListeners() {
        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Bundle arguments = new Bundle();
                arguments.putInt(POSITION, i);
                arguments.putString(MESSAGE, mData.get(i).getText());

                TemplateDialog dialog = new TemplateDialog();
                dialog.setArguments(arguments);
                dialog.show(getFragmentManager(), "");
            }
        });
    }

    private boolean isUniqueMessage(String text) {
        for (Message m : mData) {
            if (m.getText().equals(text) && text.equals(m.getText())) {
                return false;
            }
        }
        return true;
    }

    private void loadData() {
        AsyncTask<Void, Void, Void> loaderTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                MessagesDataSource eds = MessagesDataSource.getInstance();

                mData.clear();
                mData.addAll(eds.getAll());

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                mListAdapter.notifyDataSetChanged();
                mProgressBar.setVisibility(View.GONE);
                setView();
            }
        };
        loaderTask.execute();
    }

    private void saveData() {
        AsyncTask<Void, Void, Void> personDataExecutor = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                MessagesDataSource ds = MessagesDataSource.getInstance();
                ds.deleteAll();
                for (Message message : mData) {
                    ds.insert(message);
                }

                return null;
            }
        };
        personDataExecutor.execute();
    }

    private void sendToGear() {
        AsyncTask<Void, Void, Void> sender = new AsyncTask<Void, Void, Void>() {

            @Override
            protected Void doInBackground(Void... voids) {
                String result = TEMPLATE_START;
                for (Message message : mData) {
                    result += "_" + String.format(TEMPLATE_RECORD,
                            message.getText());
                }

                if (mService != null) {
                    mService.sendMessage(result);
                }
                return null;
            }
        };

        sender.execute();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.activity_messages, container, false);
        findViews(root);
        setAdapter();
        setListeners();

        if (getActivity() != null && getActivity().getActionBar() != null) {
            getActivity().getActionBar().setTitle(getResources().getString(R.string.messages_title_activity));
        }

        return root;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadData();
        sendToGear();
    }

    @Override
    public void onStart() {
        super.onStart();
        WhenAreYouComingApplication.getApplicationEventBus().register(this);
        WhenAreYouComingApplication.getAppContext().bindService(
                new Intent(WhenAreYouComingApplication.getAppContext(), WhenAreYouComingProviderService.class),
                serviceConnection,
                Context.BIND_AUTO_CREATE);

    }

    @Override
    public void onStop() {
        super.onStop();
        WhenAreYouComingApplication.getApplicationEventBus().unregister(this);
        WhenAreYouComingApplication.getAppContext().unbindService(serviceConnection);
    }

    @Subscribe
    public void refreshList(ListEvent event) {
        mListAdapter.notifyDataSetChanged();
        setView();
        sendToGear();
        saveData();
    }

    @Subscribe
    public void addMessage(AddMessageEvent event) {
        int position = event.getPosition();

        if (position > -1) {
            if (mData.get(position) != null) {
                mData.get(position).setMessage(event.getMessage());
                mListAdapter.notifyDataSetChanged();
                setView();
                sendToGear();
                saveData();
            }
        } else {
            if (isUniqueMessage(event.getMessage())) {
                mData.add(new Message(event.getMessage()));
                mListAdapter.notifyDataSetChanged();
                setView();
                sendToGear();
                saveData();
            } else {
                Toast.makeText(getActivity(), "Message is non unique!", Toast.LENGTH_SHORT).show();
            }
        }
    }
}
