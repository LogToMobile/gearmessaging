package com.logtomobile.whenareyoucoming.event;

/**
 *@author Tomasz Trybała
 */
public class ConnectionEvent {
    private boolean mConnection;

    public ConnectionEvent(boolean connection){
        mConnection = connection;
    }

    public boolean getConnection(){
        return mConnection;
    }
}
