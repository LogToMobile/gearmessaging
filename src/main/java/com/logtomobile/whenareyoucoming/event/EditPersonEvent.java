package com.logtomobile.whenareyoucoming.event;

/**
 *@author Tomasz Trybała
 */
public class EditPersonEvent {
    private String mName;
    private int mPosition;

    public EditPersonEvent(String name, int position){
        mName = name;
        mPosition = position;
    }

    public String getName(){return mName;}

    public int getPosition(){return mPosition;}
}
