package com.logtomobile.whenareyoucoming.event;

/**
 *@author Tomasz Trybała
 */
public class AddMessageEvent {
    private String mText;
    private int mPosition;

    public AddMessageEvent(String text, int position){
        mText = text;
        mPosition = position;
    }

    public String getMessage(){
        return mText;
    }

    public int getPosition(){
        return mPosition;
    }
}
