package com.logtomobile.whenareyoucoming;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.TextView;

import com.logtomobile.whenareyoucoming.event.AddMessageEvent;

/**
 * @author Tomasz Trybała
 */
public class TemplateDialog extends DialogFragment {
    private static final String MESSAGE = "message";
    private static final String POSITION = "position";
    private int mPosition = -1;

    private String setMessage(){
        Bundle arguments = getArguments();
        if(arguments != null){
            String message = arguments.getString(MESSAGE);
            if(message != null){
                return message;
            }else{
                return "";
            }
        }else{
            return "";
        }
    }

    private void setPosition(){
        Bundle arguments = getArguments();
        if (arguments != null && arguments.containsKey(POSITION)) {
            mPosition = arguments.getInt(POSITION);
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        @SuppressLint("InflateParams")
        View dialogView = inflater.inflate(R.layout.fragment_dialog_template, null, false);

        TextView btnPositive = (TextView) dialogView.findViewById(R.id.txtvPositiveButton);
        TextView btnNegative = (TextView) dialogView.findViewById(R.id.txtvNegativeButton);
        final EditText editTemplate = (EditText) dialogView.findViewById(R.id.editTemplate);

        editTemplate.setText(setMessage());
        setPosition();

        btnPositive.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (TextUtils.isEmpty(editTemplate.getText())) {
                            editTemplate.setError("Empty template message");
                        } else {
                            dismiss();
                            WhenAreYouComingApplication.getApplicationEventBus().
                                    post(new AddMessageEvent(editTemplate.getText().toString(),
                                            mPosition));
                        }
                    }
                }
        );

        btnNegative.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dismiss();
                    }
                }
        );

        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getActivity());
        dialogBuilder.setView(dialogView);

        AlertDialog dialog = dialogBuilder.create();
        dialog.setCancelable(false);
        dialog.getWindow().clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE |
                WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM);

        this.setCancelable(false);
        return dialog;
    }
}
