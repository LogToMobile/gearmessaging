package com.logtomobile.whenareyoucoming;

import com.logtomobile.whenareyoucoming.db.enitity.Person;

/**
 *@author Tomasz Trybała
 */
public interface AddController {
    void addPerson(Person p);
}
