package com.logtomobile.whenareyoucoming.net;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.util.Collections;
import java.util.Map;
import java.util.UUID;
import java.util.WeakHashMap;
import java.util.concurrent.CancellationException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

/**
 * @author Marcin Przepiórkowski
 */
public final class RequestExecutor {
    public static final int MAX_THREADS_COUNT = 4;

    private static String TAG = "RequestExecutor";

    private static ExecutorService sRequestExecutor = Executors.newFixedThreadPool(MAX_THREADS_COUNT);
    private static ExecutorService sExceptionHandler = Executors.newFixedThreadPool(MAX_THREADS_COUNT);

    private static Map<String, Future<RequestResult>> sCurrentFutureTasksMap = Collections.synchronizedMap(
            new WeakHashMap<String, Future<RequestResult>>());
    private static Map<String, CancelableTask<RequestResult>> sCurrentTasksMap = Collections.synchronizedMap(
            new WeakHashMap<String, CancelableTask<RequestResult>>());

    /**
     * Executes asynchronously given request. If an exception occurs, error data is posted on the
     * EventBus via NetErrorEvent.
     *
     * @param c             task that is to be executed
     * @param requestUri    request uri
     * @return              the task id that can be used later to cancel it
     */
    public static String execute(final CancelableTask<RequestResult> c, final String requestUri) {
        final Future<RequestResult> future = sRequestExecutor.submit(c);

        final UUID taskId = UUID.randomUUID();
        final String strTaskId = taskId.toString();

        sCurrentFutureTasksMap.put(strTaskId, future);
        sCurrentTasksMap.put(strTaskId, c);

        sExceptionHandler.execute(
                new Runnable() {
                    @Override
                    public void run() {
                        try {
                            RequestResult result = future.get();
                            Log.d(TAG, result.getMethodName() + ": " + result.getResponseCode() +
                                    ", message: " + result.getResponseMessage() + ": " + result.getRequestUri());
                        } catch (final ExecutionException exc) {
                            Handler uiHandler = new Handler(Looper.getMainLooper());
                            uiHandler.post(
                                new Runnable() {
                                    @Override
                                    public void run() {
//                                        NetErrorEvent event = new NetErrorEvent(exc.getMessage(), exc);
//                                        EventBus bus = GlisserApplication.getEventBus();
//                                        bus.post(event);
                                    }
                                }
                            );
                        } catch (CancellationException ce) {
                            Log.d(TAG, "execution cancelled, uri: " + requestUri);
                        } catch (InterruptedException iexc) {
                            Log.d(TAG, "callable execution interrupted, uri: " + requestUri);
                        }

                        sCurrentFutureTasksMap.remove(strTaskId);
                    }
                }
        );

        return strTaskId;
    }
}