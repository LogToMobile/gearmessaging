package com.logtomobile.whenareyoucoming.net;

import java.util.concurrent.Callable;

/**
 * @author Marcin Przepiórkowski
 * @param <TaskResult>  task result type
 */
public interface CancelableTask<TaskResult> extends Callable<TaskResult> {
    void cancel();
}