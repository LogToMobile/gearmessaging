package com.logtomobile.whenareyoucoming.net;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.concurrent.CancellationException;

/**
 * @author Marcin Przepiórkowski
 */
public class UploadFile {
    public final static class Response {
        private final String mResponseMessage;
        private final int mResponseCode;

        public Response(int code, String msg) {
            mResponseMessage = msg;
            mResponseCode = code;
        }

        @SuppressWarnings("UnusedDeclaration")
        public int getResponseCode() {
            return mResponseCode;
        }

        public String getResponseMessage() {
            return mResponseMessage;
        }
    }

    public UploadFile() {
    }

    public String execute(final String requestUri, final File file, final OnRequestFinishedListener<UploadFile.Response> listener) throws FileNotFoundException {

        if (!file.exists()) {
            throw new FileNotFoundException(file.getAbsolutePath());
        }

        CancelableTask<RequestResult> requestTask = new CancelableTask<RequestResult>() {
            private volatile boolean mCancel;

            @Override
            public RequestResult call() throws Exception {
                android.os.Process.setThreadPriority(android.os.Process.THREAD_PRIORITY_BACKGROUND);

                HttpURLConnection connection = null;
                int responseCode = 400;
                String responseMessage = "";

                try {
                    URL serviceUrl = new URL(requestUri);
                    connection = (HttpURLConnection) serviceUrl.openConnection();

                    connection.setConnectTimeout(Constants.CONNECTION_TIMEOUT);
                    connection.setReadTimeout(Constants.READ_TIMEOUT);
                    connection.setRequestProperty("ENCTYPE", "multipart/form-data");
                    connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=*****");
                    connection.setRequestMethod("POST");

                    connection.setDoOutput(true);
                    FileInputStream fileInputStream = new FileInputStream(file.getAbsolutePath());

                    int bytesRead, bytesAvailable, bufferSize;
                    byte[] buffer;
                    int maxBufferSize = 1024 * 1024;
                    String lineEnd = "\r\n";
                    String twoHyphens = "--";
                    String boundary = "*****";

                    DataOutputStream dos = new DataOutputStream(connection.getOutputStream());

                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Type: text/plain\r\n");
                    dos.writeBytes(("Content-Disposition: form-data; name=\"" + "action"
                            + "\"\r\n"));
                    dos.writeBytes(("\r\n" + "asr" + "\r\n"));

                    dos.writeBytes(twoHyphens + boundary + lineEnd);
                    dos.writeBytes("Content-Disposition: form-data; name=\"" + "speech"
                            + "\";filename=\"" + file.getName() + "\"" + lineEnd);

                    dos.writeBytes(lineEnd);
                    dos.flush();

                    bytesAvailable = fileInputStream.available();
                    bufferSize = Math.min(bytesAvailable, maxBufferSize);
                    buffer = new byte[bufferSize];
                    bytesRead = fileInputStream.read(buffer, 0, bufferSize);

                    while (bytesRead > 0) {
                        if (mCancel) {
                            responseCode = 400;
                            fileInputStream.close();
                            dos.close();

                            throw new CancellationException("cancel task: ");
                        }

                        dos.write(buffer, 0, bufferSize);
                        dos.flush();

                        bytesAvailable = fileInputStream.available();
                        bufferSize = Math.min(bytesAvailable, maxBufferSize);
                        bytesRead = fileInputStream.read(buffer, 0, bufferSize);
                    }

                    dos.writeBytes(lineEnd);
                    dos.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);
                    dos.flush();

                    fileInputStream.close();
                    dos.flush();
                    dos.close();

                    responseCode = connection.getResponseCode();

                    if (responseCode < 400) {
                        InputStream is = connection.getInputStream();
                        responseMessage = NetUtils.getRequestResponse(is);
                    } else {
                        InputStream is = connection.getErrorStream();
                        responseMessage = NetUtils.getRequestResponse(is);
                        is.close();
                    }
                } finally {
                    if (connection != null) {
                        connection.disconnect();
                    }

                    if (listener != null) {
                        final int code;

                        if (responseMessage.contains("ERROR")) {
                            code = 400;
                        } else {
                            code = responseCode;
                        }

                        final String msg = responseMessage;

                        Handler handler = new Handler(Looper.getMainLooper());
                        handler.post(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        listener.onRequestFinished(code,
                                                new Response(code, msg), false);
                                    }
                                }
                        );
                    }
                }

                return new RequestResult("upload file", requestUri, responseMessage, responseCode);
            }

            @Override
            public void cancel() {
                mCancel = true;
            }
        };

        return RequestExecutor.execute(requestTask, requestUri);
    }
}