package com.logtomobile.whenareyoucoming.net;

/**
 * @author Marcin Przepiórkowski
 */
public class RequestResult {
    protected String mMethodName;
    protected String mRequestUri;
    protected String mResponseMessage;
    protected int mResponseCode;

    public RequestResult(String methodName, String requestUri, String responseMessage, int responseCode) {
        mMethodName = methodName;
        mRequestUri = requestUri;
        mResponseMessage = responseMessage;
        mResponseCode = responseCode;
    }

    public String getMethodName() {
        return mMethodName;
    }

    public String getRequestUri() {
        return mRequestUri;
    }

    public int getResponseCode() {
        return mResponseCode;
    }

    public String getResponseMessage() {
        return mResponseMessage;
    }
}