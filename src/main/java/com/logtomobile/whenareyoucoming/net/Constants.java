package com.logtomobile.whenareyoucoming.net;

/**
 * @author Marcin Przepiórkowski
 *
 * This class contains some constants used during the execution of http requests.
 */
public final class Constants {
    private Constants() {
    }

    public static final int CONNECTION_TIMEOUT = 15 * 1000; // 15s
    public static final int READ_TIMEOUT = 15 * 1000; // 15s
}