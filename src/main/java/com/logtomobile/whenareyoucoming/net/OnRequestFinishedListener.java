package com.logtomobile.whenareyoucoming.net;


/**
 * @author Marcin Przepiórkowski
 *
 * The listener interface that is used by the PostRequest to indicate its execution finish.
 */
public interface OnRequestFinishedListener<T> {
    /**
     * Invoked when the request finishes its execution.
     *
     * @param responseCode  http request response code
     * @param response      http request response
     */
    void onRequestFinished(int responseCode, T response, boolean canceled);
}