package com.logtomobile.whenareyoucoming.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.SmsMessage;

import com.logtomobile.whenareyoucoming.WhenAreYouComingApplication;
import com.logtomobile.whenareyoucoming.db.ContactsDataSource;
import com.logtomobile.whenareyoucoming.db.enitity.Person;

import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Tomasz Trybała
 */
public class SmsBroadcastReceiver extends BroadcastReceiver {
    private static final String EMPTY_SPACE = "";
    private static final String SMS_EXTRA = "pdus";
    private static final String DATE_FORMAT = "yyyyMMdd  kk:mm:ss";
    private static final int CHANNEL_ID = 111;
    private static final String EXPRESSIONS = "expression.txt";
    private static final String DEFAULT_ANSWER = "_template_Yes_template_No_template_I'll let you know later.";

    private ArrayList<Person> mData = new ArrayList<Person>();

    private void checkSms(String smsNumber, String message, Context context) {
        smsNumber = smsNumber.replace("-", EMPTY_SPACE);
        smsNumber = smsNumber.replace(" ", EMPTY_SPACE);

        for (Person person : mData) {
            String personNumber = person.getNumber();
            personNumber = personNumber.replace("-", EMPTY_SPACE);
            personNumber = personNumber.replace(" ", EMPTY_SPACE);
            if (smsNumber.length() > 8 && (personNumber.contains(smsNumber) || smsNumber.contains(personNumber))) {
                sendMessageToGear(message, personNumber, context);
            }
        }
    }

    private void sendMessageToGear(String message, String number, Context context) {
        if (WhenAreYouComingApplication.getMap() != null) {
            final WhenAreYouComingProviderService.HelloAccessoryProviderConnection uHandler = WhenAreYouComingApplication.getMap().get(Integer
                    .parseInt(String.valueOf(WhenAreYouComingApplication.getId())));
            if (uHandler == null) {
                return;
            }

            DateTimeFormatter formatter = DateTimeFormat.forPattern(DATE_FORMAT);
            final String text = "sms_message_" +
                    message +
                    "_number_" +
                    number +
                    "_time_" +
                    formatter.print(DateTime.now()) +
                    findAnswers(context, message);
            new Thread(new Runnable() {
                public void run() {
                    try {
                        uHandler.send(CHANNEL_ID, text.getBytes());
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }).start();
        }
    }

    private String findAnswers(Context context, String smsMessage) {
        smsMessage = processText(smsMessage);
        AssetManager assetManager = context.getAssets();
        BufferedReader bufferedReader;
        String line;
        ArrayList<String> expressions = new ArrayList<String>();
        ArrayList<String> answers = new ArrayList<String>();
        boolean isExpression = true;
        boolean isRight = false;
        try {
            bufferedReader = new BufferedReader(new InputStreamReader(assetManager.open(EXPRESSIONS)));
            while ((line = bufferedReader.readLine()) != null) {
                if (line.equals("_expression")) {
                    if(isRight){
                        String result = "";
                        for(String s: answers){
                            result += "_template_" + s;
                        }
                        return result;
                    }
                    expressions.clear();
                    isExpression = true;
                } else if (line.equals("_answer")) {
                    if(isRightExpressions(expressions, smsMessage)){
                        isRight = true;
                    }
                    isExpression = false;
                } else {
                    if(!line.equals("")) {
                        if (isExpression) {
                            line = processText(line);
                            expressions.add(line);
                        } else if (isRight) {
                            answers.add(line);
                        }
                    }
                }
            }

            if(!answers.isEmpty()){
                String result = "";
                for(String s: answers){
                    result += "_template_" + s;
                }
                return result;
            }
        } catch (IOException e) {
            //line will be not show
        }

        return DEFAULT_ANSWER;
    }

    private String processText(String text){
        text = text.toLowerCase();
        text = text.replace(".", "");
        text = text.replace("?", "");
        text = text.replace("!", "");
        text = text.replace("'", "");
        text = text.replace("-", "");
        text = text.replace(",", "");
        return text;
    }

    private boolean isRightExpressions(ArrayList<String> expressions, String smsMessage){
        Pattern pattern;
        Matcher matcher;
        for(String expr: expressions){
            pattern = Pattern.compile(String.format("(%s)", expr));
            matcher = pattern.matcher(smsMessage);
            if(matcher.find()){
                return true;
            }
        }

        return false;
    }

    private void loadData(final String address, final String body, final Context context) {
        AsyncTask<Void, Void, Void> loaderTask = new AsyncTask<Void, Void, Void>() {
            @Override
            protected Void doInBackground(Void... params) {
                ContactsDataSource eds = ContactsDataSource.getInstance();

                mData.clear();
                mData.addAll(eds.getAll());

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                checkSms(address, body, context);
            }
        };
        loaderTask.execute();
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        Bundle extras = intent.getExtras();

        if (extras != null) {
            Object[] smsExtra = (Object[]) extras.get(SMS_EXTRA);

            for (Object aSmsExtra : smsExtra) {
                SmsMessage sms = SmsMessage.createFromPdu((byte[]) aSmsExtra);

                String body = sms.getMessageBody();
                String address = sms.getOriginatingAddress();

                loadData(address, body, context);
            }
        }
    }
}
